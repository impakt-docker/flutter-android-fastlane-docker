FROM mobiledevops/flutter-sdk-image:latest 

USER root

RUN apt-get -qq update
RUN apt-get -qqy --no-install-recommends install \
    ruby-full

RUN gem install google-api-client
RUN gem install fastlane -NV